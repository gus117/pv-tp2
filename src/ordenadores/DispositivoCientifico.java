
package ordenadores;

public class DispositivoCientifico {
    private String marca;
    private String modelo;
    private String sistemaOperativo;
    private String version;
    
    public int calcularSuma(int a, int b){
        return a + b;
    }
    public int calcularResta(int a, int b){
        return a - b;
    }
    public int calcularMultiplicacion(int a, int b){
        return a * b;
    }
    public int calcularDivision(int a, int b){
        return a / b;
    }
    public int calcularFactorial(int a){
        int fac = 1;
        for(int i = 1; i <= a; i++){
            fac = fac * i;
        }
        return fac;
    }
    public int obtenerMayorOMenor(int a, int b, boolean esMayor){
        if(esMayor){
            if(a > b){
                return a;
            }
            else
            {
                return b;
            }
        }
        else{
            if(a < b){
                return a;
            }
            else{
                return b;
            }
        }
    }
    public int calcularResto(int a, int b){
        return a%b;
    }
    public void verificarParidad(int a){
        if (0 == calcularResto(a,2)){
            System.out.println("Es Par");
        }
        else{
            System.out.println("No es Par");
        }
    }
    public float calcularPotencia(float base, int expo){
        float pro = 1;
        for(int i = 1; i<=expo; i++){
            pro = pro * base;
        }
        return pro;
    }
    public float calcularCoseno(float x, int n){
        float sum = 0;
        for(int i = 1; i <= n; i++){
            sum = sum + calcularPotencia(-1,i) * (calcularPotencia(x, 2*i) / calcularFactorial(i));
        }
        return sum;
    }
}
